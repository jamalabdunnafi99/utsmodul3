import 'package:flutter/material.dart';
import './product_manager.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hello UTS',
      home: Scaffold(
        appBar: AppBar(
          title: Text('surya, 161240000544'),
        ),
        body: ProductManager(),
      ),
    );
  }
}